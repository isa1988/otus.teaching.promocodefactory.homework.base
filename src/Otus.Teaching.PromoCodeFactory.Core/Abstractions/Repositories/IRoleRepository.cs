﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRoleRepository : IRepository<Role, Guid>
    {
    }
}

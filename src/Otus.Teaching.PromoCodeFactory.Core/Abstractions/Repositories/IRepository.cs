﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> 
        where T : class, IEntity
    {
        T Create(T entity);
        //List<T> GetAllOfPage(int pageNumber, int rowCount);
        IEnumerable<T> GetAll();
        void Update(T entity);
        void Delete(T entity);
    }

    public interface IRepository<T, TId> : IRepository<T>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
    {
        T GetById(TId id);
    }
}
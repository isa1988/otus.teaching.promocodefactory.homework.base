﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public interface IEntity
    {
    }

    public interface IEntity<TId> : IEntity where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
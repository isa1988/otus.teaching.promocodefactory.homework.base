﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    static class HelpWithModifeEnumerable
    {
        public static IEnumerable<T> Add<T>(this IEnumerable<T> enumerable, T value)
        {
            return enumerable.Concat(new T[] { value });
        }
        public static IEnumerable<T> Edit<T>(this IEnumerable<T> enumerable, int index, T value)
        {
            return enumerable.Select((x, i) => index == i ? value : x);
        }
        public static IEnumerable<T> Remove<T>(this IEnumerable<T> enumerable, int index)
        {
            return enumerable.Where((x, i) => index != i);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class RepositoryBase<T> : IRepository<T> where T : class, IEntity
    {
        protected static IEnumerable<T> Data { get; set; }
        public RepositoryBase(IEnumerable<T> data)
        {
            Data = data;
        }

        protected IQueryable<T> DbSetInclude { get; set; }

        public virtual T Create(T entity)
        {
            Data = Data.Add(entity);

            return Data.Last();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Data;
        }

        //identity
        
        public virtual void Update(T entity)
        {
            int index = Data.ToList().IndexOf(entity);
            Data = Data.Edit(index, entity);
        }

        public virtual void Delete(T entity)
        {
            int index = Data.ToList().IndexOf(entity);
            Data = Data.Remove(index);
        }
    }

    public class RepositoryBase<T, TId> : RepositoryBase<T>, IRepository<T, TId>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
    {
        public RepositoryBase(IEnumerable<T> data) : base(data)
        {
        }
        public virtual T GetById(TId id)
        {
            var item = Data.FirstOrDefault(x => x.Id.Equals(id));
            if (item == null)
            {
                throw new NullReferenceException($"Запись с id {id} не найдена");
            }

            return item;
        }
    }
}

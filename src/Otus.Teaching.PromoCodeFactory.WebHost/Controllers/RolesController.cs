﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;
using Otus.Teaching.PromoCodeFactory.Services.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly IRoleService _rolesService;
        private readonly IMapper _mapper;

        public RolesController(IRoleService rolesService, IMapper mapper)
        {
            _rolesService = rolesService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageWithList> GetRolesAsync()
        {
            var result = _rolesService.GetAll();

            var model = new PageWithList();
            model.Roles = _mapper.Map<List<RoleModel>>(result);
            return model;
        }

        /// <summary>
        /// Получить данные роли по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleModel>> GetEmployeeByIdAsync(Guid id)
        {
            var result = await _rolesService.GetByID(id);

            if (result.IsSuccess)
            {
                var roleModel = _mapper.Map<RoleModel>(result.Entity);
                return roleModel;
            }
            else
            {
                return new RoleModel()
                {
                    IsFail = true,
                    Error = result.GetErrorString(),
                };
            }
        }

        /// <summary>
        /// Добавить новую роль
        /// </summary>
        /// <param name="model">роль</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<HttpResponseMessage> Add([FromBody] RoleAddModel model)
        {

            HttpResponseMessage returnMessage = new HttpResponseMessage();

            var role = _mapper.Map<RoleDto>(model);
            var result =  await _rolesService.CreateItemAsync(role);
            if(result.IsSuccess)
            {
                
                string message = ($"Student Created - {result.Entity.Id}");
                returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
            }
            else
            {
                returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, result.GetErrorString());
            }


            return returnMessage;
        }

        /// <summary>
        /// Обновить роль
        /// </summary>
        /// <param name="model">роль</param>
        /// <returns></returns>

        [HttpPost("Update")]
        public async Task<HttpResponseMessage> Edit([FromBody] RoleEditModel model)
        {

            HttpResponseMessage returnMessage = new HttpResponseMessage();

            var role = _mapper.Map<RoleDto>(model);
            var result = await _rolesService.EditItemAsync(role);
            if (result.IsSuccess)
            {

                string message = ($"Student Created - {result.Entity.Id}");
                returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
            }
            else
            {
                returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, result.GetErrorString());
            }


            return returnMessage;
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns></returns>

        [HttpPost("Delete{id:guid}")]
        public async Task<HttpResponseMessage> Delete(Guid id)
        {

            HttpResponseMessage returnMessage = new HttpResponseMessage();

            var result = await _rolesService.DeleteItemAsync(id);
            if (result.IsSuccess)
            {

                string message = ($"Student Created - {result.Entity.Id}");
                returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
            }
            else
            {
                returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, result.GetErrorString());
            }


            return returnMessage;
        }
    }
}
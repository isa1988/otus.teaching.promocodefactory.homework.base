﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PageInfo
    {
        public PageInfo()
        {
            Error = string.Empty;
        }

        public bool IsFail { get; set; }
        public string Error { get; set; }

    }
}

﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class EmployeeAddModel 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public List<RoleAddModel> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}

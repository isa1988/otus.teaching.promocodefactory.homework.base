﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Role
{
    public class RoleModel : PageInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
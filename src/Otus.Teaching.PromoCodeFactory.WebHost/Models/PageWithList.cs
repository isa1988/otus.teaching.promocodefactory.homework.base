﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PageWithList : PageInfo
    {
        public PageWithList()
        {
            Roles = new List<RoleModel>();
            Employees = new List<EmployeeModel>();
        }

        public List<RoleModel> Roles { get; set; }
        public List<EmployeeModel> Employees { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            EmployeeMapping();
            RoleMapping();
        }

        private void RoleMapping()
        {
            CreateMap<RoleAddModel, RoleDto>()
                .ForMember(x => x.Id, p => p.MapFrom(c => Guid.NewGuid()));

            CreateMap<RoleEditModel, RoleDto>();
            CreateMap<RoleDto, RoleModel>();
            CreateMap<RoleModel, RoleDto>();
        }

        private void EmployeeMapping()
        {
            CreateMap<EmployeeAddModel, EmployeeDto>()
                .ForMember(x => x.Id, p => p.MapFrom(c => Guid.NewGuid()));
            CreateMap<EmployeeEditModel, EmployeeDto>();
            CreateMap<EmployeeDto, EmployeeModel>();
            CreateMap<EmployeeModel, EmployeeDto>();
        }
    }
}

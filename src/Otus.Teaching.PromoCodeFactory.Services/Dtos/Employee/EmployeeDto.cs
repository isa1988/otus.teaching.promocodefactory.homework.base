﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;

namespace Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }
        public List<RoleDto> Roles { get; set; }
    }
}

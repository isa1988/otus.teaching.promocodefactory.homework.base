﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee
{
    public class EmployeeDeleteDto
    {
        public Guid Id { get; set; }
    }
}

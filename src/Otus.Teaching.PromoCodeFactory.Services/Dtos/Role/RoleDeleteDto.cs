﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Services.Dtos.Role
{
    public class RoleDeleteDto
    {
        public Guid Id { get; set; }
    }
}

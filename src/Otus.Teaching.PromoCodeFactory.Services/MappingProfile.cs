﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;

namespace Otus.Teaching.PromoCodeFactory.Services
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            EmployeeMapping();
            RoleMapping();
        }

        private void RoleMapping()
        {
            CreateMap<RoleDto, Role>();
            CreateMap<Role, RoleDto>();
        }

        private void EmployeeMapping()
        {
            CreateMap<EmployeeDto, Employee>();
            CreateMap<Employee, EmployeeDto>();
        }
    }
}

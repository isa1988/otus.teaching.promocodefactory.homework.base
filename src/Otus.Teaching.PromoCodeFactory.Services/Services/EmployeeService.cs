﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee;
using Otus.Teaching.PromoCodeFactory.Services.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public class EmployeeService : GeneralService<Employee, EmployeeDto, Guid>, IEmployeeService
    {
        public EmployeeService(IEmployeeRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<EntityOperationResult<Employee>> EditItemAsync(EmployeeDto basketEditDto)
        {
            string errors = CheckAndGetErrors(basketEditDto, false);
            if (!string.IsNullOrEmpty(errors))
            {
                return EntityOperationResult<Employee>.Failure().AddError(errors);
            }

            try
            {
                var value = _repositoryWithId.GetById(basketEditDto.Id);

                value.Email = basketEditDto.Email;
                value.FirstName = basketEditDto.FirstName;
                value.AppliedPromocodesCount = basketEditDto.AppliedPromocodesCount;
                value.LastName = basketEditDto.LastName;
                value.Roles = _mapper.Map<List<Role>>(basketEditDto.Roles);

                _repository.Update(value);

                return EntityOperationResult<Employee>.Success(value);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<Employee>.Failure().AddError(ex.Message);
            }
        }

        protected override string CheckAndGetErrors(EmployeeDto value, bool isNew = true)
        {
            return string.Empty;
        }

        protected override string CkeckBefforDelet(Employee value)
        {
            return string.Empty;
        }
    }
}

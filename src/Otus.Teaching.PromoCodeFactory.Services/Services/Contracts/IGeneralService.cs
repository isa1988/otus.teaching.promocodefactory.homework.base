﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Contracts
{
    public interface IGeneralService<TBase, TDto>
        where TBase : IEntity
    {


        /// <summary>
        /// Вернуть все записи
        /// </summary>
        /// <returns></returns>
        List<TDto> GetAll();

    }

    public interface IGeneralService<TBase, TDto, TId> : IGeneralService<TBase, TDto>
        where TBase : IEntity
        where TId : IEquatable<TId>
    {

        /// <summary>
        /// Добавить запись в базу
        /// </summary>
        /// <param name="basketCreateDto">Объект добавление</param>
        /// <returns></returns>
        Task<EntityOperationResult<TBase>> CreateItemAsync(TDto basketCreateDto);

        /// <summary>
        /// Удалить запись из базы
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TBase>> DeleteItemAsync(TId id);

        /// <summary>
        /// Вернуть конкретный объект из базы
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> GetByID(TId id);
    }
}

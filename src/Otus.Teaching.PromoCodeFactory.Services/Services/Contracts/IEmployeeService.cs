﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Employee;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Contracts
{
    public interface IEmployeeService : IGeneralService<Employee, EmployeeDto, Guid>
    {
        public Task<EntityOperationResult<Employee>> EditItemAsync(EmployeeDto basketEditDto);
    }
}

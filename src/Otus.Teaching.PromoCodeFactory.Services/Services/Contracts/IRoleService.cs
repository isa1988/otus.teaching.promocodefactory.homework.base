﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;

namespace Otus.Teaching.PromoCodeFactory.Services.Services.Contracts
{
    public interface IRoleService : IGeneralService<Role, RoleDto, Guid>
    {
        public Task<EntityOperationResult<Role>> EditItemAsync(RoleDto basketEditDto);
    }
}

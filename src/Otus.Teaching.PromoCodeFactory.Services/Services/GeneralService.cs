﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public abstract class GeneralService<TBase, TDto> : IGeneralService<TBase, TDto>
        where TBase : class, IEntity
    {
        public GeneralService(IRepository<TBase> repository, IMapper mapper)
        {
            if (repository == null)
                throw new ArgumentNullException(nameof(repository));

            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _repository = repository;
            _mapper = mapper;
        }

        protected readonly IRepository<TBase> _repository;
        protected readonly IMapper _mapper;


        public virtual List<TDto> GetAll()
        {
            IEnumerable<TBase> valueBaseList = _repository.GetAll();
            if (valueBaseList == null || valueBaseList.Count() == 0)
            {
                return new List<TDto>();
            }

            List<TDto> retList = _mapper.Map<List<TDto>>(valueBaseList.ToList());
            return retList;
        }
    }


    public abstract class GeneralService<TBase, TDto, TId> : GeneralService<TBase, TDto>,
        IGeneralService<TBase,TDto,TId>
        where TBase : class, IEntity<TId>
        where TId : IEquatable<TId>
    {
        public GeneralService(IRepository<TBase, TId> repository, IMapper mapper)
            : base(repository, mapper)
        {
            _repositoryWithId = repository;
        }
        protected readonly IRepository<TBase, TId> _repositoryWithId;
        protected abstract string CheckAndGetErrors(TDto value, bool isNew = true);

        public async Task<EntityOperationResult<TBase>> CreateItemAsync(TDto basketCreateDto)
        {
            string errors = CheckAndGetErrors(basketCreateDto);
            if (!string.IsNullOrEmpty(errors))
            {
                return EntityOperationResult<TBase>.Failure().AddError(errors);
            }

            try
            {
                TBase value = _mapper.Map<TBase>(basketCreateDto);

                var entity = _repository.Create(value);

                return EntityOperationResult<TBase>.Success(entity);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TBase>.Failure().AddError(ex.Message);
            }
        }

        protected abstract string CkeckBefforDelet(TBase value);

        public async Task<EntityOperationResult<TBase>> DeleteItemAsync(TId id)
        {
            try
            {
                TBase value = _repositoryWithId.GetById(id);
                if (value == null)
                {
                    return EntityOperationResult<TBase>.Failure().AddError("Не найдена запись");
                }

                string error = CkeckBefforDelet(value);
                if (!string.IsNullOrEmpty(error))
                {
                    return EntityOperationResult<TBase>.Failure().AddError(error);
                }

                _repository.Delete(value);

                return EntityOperationResult<TBase>.Success(value);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TBase>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> GetByID(TId id)
        {
            try
            {
                TBase value = _repositoryWithId.GetById(id);
                if (value == null)
                    return EntityOperationResult<TDto>.Failure().AddError("Запись не найдена");
                TDto dto = _mapper.Map<TDto>(value);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

    }
}
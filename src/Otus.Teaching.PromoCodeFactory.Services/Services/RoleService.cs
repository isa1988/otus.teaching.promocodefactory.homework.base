﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Dtos.Role;
using Otus.Teaching.PromoCodeFactory.Services.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Services
{
    public class RoleService : GeneralService<Role, RoleDto, Guid>, IRoleService
    {
        public RoleService(IRoleRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<EntityOperationResult<Role>> EditItemAsync(RoleDto basketEditDto)
        {
            string errors = CheckAndGetErrors(basketEditDto, false);
            if (!string.IsNullOrEmpty(errors))
            {
                return EntityOperationResult<Role>.Failure().AddError(errors);
            }

            try
            {
                var value = _repositoryWithId.GetById(basketEditDto.Id);
                value.Description = basketEditDto.Description;
                value.Name = basketEditDto.Name;

                _repository.Update(value);

                return EntityOperationResult<Role>.Success(value);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<Role>.Failure().AddError(ex.Message);
            }
        }

        protected override string CheckAndGetErrors(RoleDto value, bool isNew = true)
        {
            return string.Empty;
        }

        protected override string CkeckBefforDelet(Role value)
        {
            return string.Empty;
        }
    }
}
